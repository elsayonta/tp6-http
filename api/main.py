from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel


class Mot(BaseModel):
    id:int
    caracteres:str

mots = [Mot(id = 0, caracteres = 'mot1'), Mot(id = 1, caracteres = 'mot2')]

app = FastAPI()

@app.get("/mots")
def get_all_mots():
         return mots

@app.post("/mot")
async def add_mot(mot:Mot):
    mots.append(mot)
    return mot
