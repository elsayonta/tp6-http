## Installer les dépendances:
`pip3 install -r requirements.txt`

## Lancer l'application:
`uvicorn main:app --reload`

## Accéder à l'application:
